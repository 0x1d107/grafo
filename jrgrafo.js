class Color{
    constructor(r,g,b,a){
        this.r=r;
        this.g=g;
        this.b=b;
        this.a=a;
    }
    rgba(){
        return "rgba("+this.r+","+this.g+","+this.b+","+this.a+")";
    }
}
class Drawable{
    constructor(x,y){
        this.x=x;
        this.y=y;
        this.color=new Color(0,0,0,255);
        this.rotation=0;
        this.children=[];
        this.namedChildren={};
    }
    setPosition(x,y){
        this.x=x;
        this.y=y;
        return this;
    }
    setRotation(rad){
        this.rotation=rad;
        return this;
    }
    setColor(r,g,b,a){
        this.color = new Color(r,g,b,a);
        return this;
    }
    addWithNamed(fn){
        return this.addShape(fn(this.namedChildren));
    }
    addShape(drawable){
        this.children.push(drawable);
        Object.assign(this.namedChildren,drawable.namedChildren);
        if(drawable.name!==undefined)
            this.namedChildren[drawable.name]=drawable;
        return this;
    }
    draw_with_children(ctx){
        this.draw(ctx);
        for(var drawable of this.children){
            ctx.save();
            ctx.translate(drawable.x,drawable.y);
            ctx.rotate(drawable.rotation);
            ctx.beginPath()
            ctx.fillStyle = drawable.color.rgba();
            ctx.strokeStyle = drawable.color.rgba();
            drawable.draw(ctx);
            ctx.restore();
        }
    }
    draw(ctx){
        throw Error("Method is not implemented");
    }
    
}
class Drawer{
    constructor(canvas,array_of_drawables=[]){
        this.canvas = canvas;
        this.width = canvas.width;
        this.height = canvas.height;
        this.ctx = canvas.getContext("2d");
        this.drawables = array_of_drawables;
        this.namedDrawables={};
    }
    addShape(drawable){
        this.drawables.push(drawable);
        Object.assign(this.namedDrawables,drawable.namedChildren);
        if(drawable.name!==undefined)
            this.namedDrawables[drawable.name]=drawable;
        return this;
    }
    addWithNamed(fn){
        
        return this.addShape(fn(this.namedDrawables));
    }
    draw(){
        this.ctx.clearRect(0,0,this.width,this.height);
        for(var drawable of this.drawables){
            this.ctx.save();
            this.ctx.translate(drawable.x,drawable.y);
            this.ctx.rotate(drawable.rotation);
            this.ctx.beginPath()
            this.ctx.fillStyle = drawable.color.rgba();
            this.ctx.strokeStyle = drawable.color.rgba();
            drawable.draw_with_children(this.ctx);
            this.ctx.restore();
        }
        return this;
    }

}