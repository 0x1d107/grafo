class Point extends Drawable{
    constructor(x,y,r,name){
        super(x,y);
        this.r= r;
        this.name=name;
    }
    draw(ctx){
        ctx.arc(0,0,this.r,0,2*Math.PI);
        ctx.fill();
        ctx.fillStyle = "#ffffff";
        ctx.font = 1.5*this.r+"px serif"
        ctx.fillText(this.name,-this.r/2,this.r/2);
    }
}

class Line extends Drawable{
    constructor(p1,p2){
        super((p1.x+p2.x)/2,(p1.y+p2.y)/2);
        this.p1=p1;
        this.p2=p2;
        this.r= (p1.r+p2.r)/4;
        this.name=p1.name+p2.name;
        
    }
    draw(ctx){
        var x1 = (this.p1.x-this.p2.x)/2;
        var y1 = (this.p1.y-this.p2.y)/2;
        var x2 = -x1;
        var y2 = -y1;
        var l = Math.sqrt(4*x1*x1+4*y1*y1);
        x1-=x1*2*this.p1.r/l;
        y1-=y1*2*this.p1.r/l;
        x2-=x2*2*this.p2.r/l;
        y2-=y2*2*this.p2.r/l;
        ctx.moveTo(x1,y1);
        ctx.lineTo(x2,y2);
        ctx.lineWidth = this.r;
        ctx.stroke();
    }
}
class Axis extends Drawable{
    constructor(x1,y1,x2,y2,r,ratio,start=0,step=1,name=undefined,drawZero=true,exp=1){
        super((x1+x2)/2,(y1+y2)/2);
        this.name = name;
        this.x1 = (x1-x2)/2;
        this.y1 = (y1-y2)/2;
        this.x2=-this.x1;
        this.y2=-this.y1;
        this.start= start
        this.step = step;
        this.exp=exp;
        this.r=r;
        this.drawZero=drawZero;
        this.ratio=ratio;
        this.len = Math.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
        this.unitX = (x2-x1)/this.len*ratio;
        this.unitY = (y2-y1)/this.len*ratio;

    }
    draw(ctx){
        
        ctx.lineWidth = this.r;
        var per_x1 =  -2* this.unitY*this.r/this.ratio;
        var per_x2 =   2* this.unitY*this.r/this.ratio;
        var per_y1 =   2* this.unitX*this.r/this.ratio;
        var per_y2 =  -2* this.unitX*this.r/this.ratio;
        var top_x = 2*this.unitX*this.r/this.ratio;
        var top_y = 2*this.unitY*this.r/this.ratio;
        for(var i=0,x=this.x1,y=this.y1;i<this.len/this.ratio;i++,x+=this.unitX,y+=this.unitY){
            ctx.beginPath();
            
            
            ctx.moveTo(x+per_x1,y+per_y1);
            
            ctx.lineTo(x+per_x2,y+per_y2);
            if(this.drawZero||(i*this.step+this.start)*this.exp**i){
                ctx.font = 5*this.r+"px serif";
                ctx.fillText((i*this.step+this.start)*this.exp**i,x-per_x2*2,y-per_y2*3);
            }
            ctx.stroke();
        }
        ctx.moveTo(this.x1,this.y1);
        ctx.lineTo(this.x2,this.y2);
        ctx.stroke();
        ctx.closePath()
        ctx.beginPath();
        ctx.moveTo(this.x2+per_x1,this.y2+per_y1);
        ctx.lineTo(this.x2+per_x2,this.y2+per_y2);
        
        ctx.lineTo(this.x2+top_x,this.y2+top_y);
        

        ctx.fill();
        ctx.font = 7*this.r+"px serif";
        if(this.name)ctx.fillText(this.name,x-per_x2*2,y-per_y2*3);
    }
}
//REF https://yacas.readthedocs.io/en/latest/book_of_algorithms/basic.html#adaptive-function-plotting
/*
 * Returns array of critical values for argument
 */
function adaptive_plot(fx,xrange,depth,epsilon){
    var a = xrange[0];
    var c = xrange[1];
    var b = (a+c)/2;
    var a1 = (a+b)/2;
    var b1 = (b+c)/2;
    var points = [a,a1,b,b1,c];
    var values = points.map(fx);
    if (depth<=0)
        return points;
    function isNotOscillatingRapidly(v){
        var score = 0;
        for(var i=0;i<v.length-2;i++){
            var a = v[0];
            var b = v[1];
            var c = v[2];
            if(![a,b,c].every(isFinite)||b<a&&b<c||b>a&&b>c)
                score++;
            
        }
        return score<3;
    }
    function areValuesSmooth(values,fn,e){//TODO
        var gx = (x=>fn(x)-Math.min.apply(this,values));
        function getIntegralApprox(fn,values,coefficients){
            var h = values[1]-values[0];
            return h*values.reduce((a,x,i)=>a+gx(x)*coefficients[i],0);
        }
        var q1 = [3/8,19/24,-5/24,1/24];
        var q2 = [5/12,2/3,-1/12];
        
        var v1 = values.slice(0,4);
        var v2 = values.slice(2,5);

        var i1=getIntegralApprox(fn,v1,q1);
        var i2 =getIntegralApprox(fn,v2,q2);
        return Math.abs(i1-i2)<e*i2;
    }
    
    if(isNotOscillatingRapidly(values)&&areValuesSmooth(values,fx,epsilon)){
        return values;
    }else{
        //Refine grid
        return [].concat(adaptive_plot(fx,[a,b],depth-1,epsilon*2),adaptive_plot(fx,[b,c],depth-1,epsilon*2));

    }


}
class AdaptivePlot extends Drawable{
    constructor(x,y,r,fx,fy,tInterval=[0,1],epsilon=1,xInterval=[-300,300],yInterval=[-300,300]){
        super(x,y);
        this.r=r;
        this.fx= fx;
        this.fy = fy;
        this.interval = tInterval;
        this.xInterval = xInterval;
        this.yInterval = yInterval;
        this.epsilon = Math.abs(epsilon/(xInterval[1]-xInterval[0])/(yInterval[1]-yInterval[0]));
        this.precision = 8;
    }
    draw(ctx){
        function is_in_interval(int,x){
            return int[0]<=x&&x<=int[1];
        }
        function are_coords_in_bounds(self,x,y){
            return is_in_interval(self.yInterval,y)&&is_in_interval(self.xInterval,x);
        }
        ctx.beginPath();
        ctx.lineWidth = this.r;
        var t_values =new Set([].concat(adaptive_plot(this.fx,this.interval,this.precision,this.epsilon),adaptive_plot(this.fy,this.interval,this.precision,this.epsilon)).sort((a,b)=>a-b));
        var ox = this.fx(this.interval[0]);
        var oy = this.fy(this.interval[0]);

        for(var t of t_values){
            var x = this.fx(t);
            var y = this.fy(t);

            if(are_coords_in_bounds(this,x,y)||are_coords_in_bounds(this,ox,oy)){
                ctx.lineTo(x,y);
                ctx.moveTo(x,y);
                
            }else{
                ctx.beginPath();
                ctx.moveTo(x,y);
                
                console.log("jump");
            }
            ctx.stroke();
           // console.log(x,y);
        }
    }
}
class SimplePlot extends Drawable{
    constructor(x,y,r,fx,fy,tInterval=[0,1],epsilon=0.001,xInterval=[-300,300],yInterval=[-300,300]){
        super(x,y);
        this.r=r;
        this.fx= fx;
        this.fy = fy;
        this.interval = tInterval;
        this.xInterval = xInterval;
        this.yInterval = yInterval;
        this.epsilon = epsilon;
        this.precision = 10;
    }
    draw(ctx){
        function is_in_interval(int,x){
            return int[0]<=x&&x<=int[1];
        }
        function are_coords_in_bounds(self,x,y){
            return is_in_interval(self.yInterval,y)&&is_in_interval(self.xInterval,x);
        }
        function bin_search_edge(self,ox,oy,ot,x,y,t,precision){
            while(Math.max(Math.abs((y-oy)),Math.abs((x-ox)))>2*precision){

                var mt = (t+ot)/2;
                var mx = self.fx(mt);
                var my = self.fy(mt);
                if(are_coords_in_bounds(self,x,y)==are_coords_in_bounds(self,mx,my)){
                    x = mx;
                    y = my;
                    t = mt;
                }else if (are_coords_in_bounds(self,ox,oy)==are_coords_in_bounds(self,mx,my)){
                    ox = mx;
                    oy = my;
                    ot = mt;
                }
            }
            return are_coords_in_bounds(self,x,y)?t:ot;
        }
        ctx.beginPath();
        ctx.lineWidth = this.r;
        var ox = this.fx(this.interval[0]);
        var oy = this.fy(this.interval[0]);
        
        var e = this.epsilon;
        for(var t = this.interval[0];t<this.interval[1];t+=e){
            var x = this.fx(t);
            var y = this.fy(t);
            
            if(are_coords_in_bounds(this,ox,oy)!=are_coords_in_bounds(this,x,y)){
                //console.log(x,y);
                var edge_t = bin_search_edge(this,ox,oy,t-this.epsilon,x,y,t,0.1);
                var edge_x = this.fx(edge_t);
                var edge_y = this.fy(edge_t);
                ctx.lineTo(edge_x,edge_y);
                ctx.moveTo(edge_x,edge_y);
                ctx.stroke();
            }
            if(are_coords_in_bounds(this,x,y)){
                ctx.lineTo(x,y);
                ctx.moveTo(x,y);
                e = this.epsilon / (Math.min(this.epsilon*this.precision,Math.max(Math.abs((y-oy)/(e)),Math.abs((x-ox)/(e)),1)));
            }else{
               // console.log("Skip");
                e = this.epsilon;
                ctx.beginPath();
            }
                
            ctx.stroke();
          
            e= !isFinite(e)?this.epsilon:e;
            //console.log(this.epsilon,e);
            ox=x;
            oy=y;
            
            

        }
        ctx.stroke();
    }
}
class Plotter{
    constructor(drawer,xRatio=20,yRatio=20,r=4,shiftOriginX=0,shiftOriginY=0){
        this.drawer=drawer;
        this.funcs=[];
        this.r = r;
        this.xRatio = xRatio;
        this.yRatio = yRatio;
        this.originX=drawer.width/2+shiftOriginX*drawer.width/xRatio;
        this.originY=drawer.height/2-shiftOriginY/yRatio*drawer.height;
        this.xInterval = [-(this.originX/this.drawer.width)*xRatio,-(this.originX/this.drawer.width)*xRatio+xRatio];
        this.yInterval = [-(1-this.originY/this.drawer.height)*yRatio,-(1-this.originY/this.drawer.height)*yRatio+yRatio];
       }

    /*
    params = {
        x : function(t){return x_coordinate}, defaults to function(t){return t;}
        y : function(t){return y_coordinate}
        t : array of two int, interval of parameter values that will be interpolated, defaults to visible x axis interval
        epsilon : rate of change of t
    }
    */
    transformX(x){//from point to pixels relative to origin
        return x*this.drawer.width/this.xRatio;
    }
    transformY(y){//from point to pixels relative to origin
        return -y*this.drawer.height/this.yRatio;
    }
    transformFunctions(funcs){
        var tx = this.transformX.bind(this);
        var ty = this.transformY.bind(this);
        var ox = funcs.x;
        var oy = funcs.y;
        funcs.x = function(x){return tx(ox(x))};
        funcs.y = function(y){return ty(oy(y))};
        return funcs;
    }
    addFunction(params){
        var par = {
            x:function(t){return t;},
            t:Array.from(this.xInterval)
        }
        if(typeof params == "function")
            par.y = params;
        else
            Object.assign(par, params);
        
        this.funcs.push(this.transformFunctions( par));
        return this;
    }
    plot(pltr=AdaptivePlot){
        if(this.yRatio)this.drawer.addShape(new Axis(this.originX,this.drawer.height,this.originX,0,this.r,this.drawer.height/this.yRatio,-(1-this.originY/this.drawer.height)*this.yRatio,1,"Y",false));
        if(this.xRatio)this.drawer.addShape(new Axis(0,this.originY,this.drawer.width,this.originY,this.r,this.drawer.width/this.xRatio,-(this.originX/this.drawer.width)*this.xRatio,1,"X",false));
        for(var f of this.funcs){
            var e = 1/this.drawer.width/this.drawer.height;
            this.drawer.addShape(new pltr(this.originX,this.originY,this.r,f.x,f.y,f.t,e,this.xInterval.map(x=>x*this.drawer.width/this.xRatio),this.yInterval.map(y=>y*this.drawer.height/this.yRatio)));
        }
        return this.drawer;
    }
}