//REF https://yacas.readthedocs.io/en/latest/book_of_algorithms/basic.html#adaptive-function-plotting
/*
 * Returns array of critical values for argument
 */
function adaptive_plot(fx,xrange,depth,epsilon){
    var a = xrange[0];
    var c = xrange[1];
    var b = (a+c)/2;
    var a1 = (a+b)/2;
    var b1 = (b+c)/2;
    var points = [a,a1,b,b1,c];
    var values = points.map(fx);
    if (depth<=0)
        return points;
    function isNotOscillatingRapidly(v){
        var score = 0;
        for(var i=0;i<v.length-2;i++){
            var a = v[0];
            var b = v[1];
            var c = v[2];
            if(![a,b,c].every(isFinite)||b<a&&b<c||b>a&&b>c)
                score++;
            
        }
        return score<3;
    }
    function areValuesSmooth(values,fn,e){//TODO
        var gx = (x=>fn(x)-Math.min.apply(this,values));
        function getIntegralApprox(fn,values,coefficients){
            var h = values[1]-values[0];
            return h*values.reduce((a,x,i)=>a+gx(x)*coefficients[i],0);
        }
        var q1 = [3/8,19/24,-5/24,1/24];
        var q2 = [5/12,2/3,-1/12];
        
        var v1 = values.slice(0,4);
        var v2 = values.slice(2,5);

        var i1=getIntegralApprox(fn,v1,q1);
        var i2 =getIntegralApprox(fn,v2,q2);
        return Math.abs(i1-i2)<e*i2;
    }
    
    if(isNotOscillatingRapidly(values)&&areValuesSmooth(values,fx,epsilon)){
        return values;
    }else{
        //Refine grid
        return [].concat(adaptive_plot(fx,[a,b],depth-1,epsilon*2),adaptive_plot(fx,[b,c],depth-1,epsilon*2));

    }


}
class AdaptivePlot extends Drawable{
    constructor(x,y,r,fx,fy,tInterval=[0,1],epsilon=1,xInterval=[-300,300],yInterval=[-300,300]){
        super(x,y);
        this.r=r;
        this.fx= fx;
        this.fy = fy;
        this.interval = tInterval;
        this.xInterval = xInterval;
        this.yInterval = yInterval;
        this.epsilon = Math.abs(epsilon/(xInterval[1]-xInterval[0])/(yInterval[1]-yInterval[0]));
        this.precision = 8;
    }
    draw(ctx){
        function is_in_interval(int,x){
            return int[0]<=x&&x<=int[1];
        }
        function are_coords_in_bounds(self,x,y){
            return is_in_interval(self.yInterval,y)&&is_in_interval(self.xInterval,x);
        }
        ctx.beginPath();
        ctx.lineWidth = this.r;
        var t_values =new Set([].concat(adaptive_plot(this.fx,this.interval,this.precision,this.epsilon),adaptive_plot(this.fy,this.interval,this.precision,this.epsilon)).sort((a,b)=>a-b));
        var ox = this.fx(this.interval[0]);
        var oy = this.fy(this.interval[0]);

        for(var t of t_values){
            var x = this.fx(t);
            var y = this.fy(t);

            if(are_coords_in_bounds(this,x,y)||are_coords_in_bounds(this,ox,oy)){
                ctx.lineTo(x,y);
                ctx.moveTo(x,y);
                
            }else{
                ctx.beginPath();
                ctx.moveTo(x,y);
                
                console.log("jump");
            }
            ctx.stroke();
           // console.log(x,y);
        }
    }
}