console.time("total");
var p = new Drawer(document.getElementById("cnvs"));
var plotter = new Plotter(p,30,30,2,0,-0);
plotter.addFunction(x=>Math.tan(3*x/5))
.plot(AdaptivePlot)
.draw();
console.timeEnd("total");